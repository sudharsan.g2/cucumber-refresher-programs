Feature: Demo web shop validation

  Scenario: Verify the demo web shop login functionality
    Given Open the website "https://demowebshop.tricentis.com/"
    
    Then The home page of the website get displayed
    
    When Click on the login profile button
    
    And Enter the email "davidbilla07@gmail.com" in the email textbox.
    
    And Enter the password "David@07" in the password textbox.
    
    And click on the login button
    
    Then the home page with loged user name should dispay
    
    And click the logout button
    
    Then The user should taken to the homepage of the demo web shop
    
    And close the browser

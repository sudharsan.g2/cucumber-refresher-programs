package stepDefinitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "/home/sudharsan/Documents/Seleniumrefresher/CucumberPra/src/test/resources/features/Login.feature",
		glue = "stepDefinitions"
		)
public class RunnerClass {

}

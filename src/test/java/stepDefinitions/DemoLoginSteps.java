package stepDefinitions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

public class DemoLoginSteps {

	static WebDriver driver;

	@Given("Open the website {string}")
	public void open_the_website(String string) {
		driver = new ChromeDriver();
		driver.get(string);
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

	@Then("The home page of the website get displayed")
	public void the_home_page_of_the_website_get_displayed() {
		System.out.println("HomePage");
	}

	@When("Click on the login profile button")
	public void click_on_the_login_profile_button() {
		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
	}

	@When("Enter the email {string} in the email textbox.")
	public void enter_the_email_in_the_email_textbox(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("Enter the password {string} in the password textbox.")
	public void enter_the_password_in_the_password_textbox(String string) {
		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
	}

	@Then("the home page with loged user name should dispay")
	public void the_home_page_with_loged_user_name_should_dispay() {
		System.out.println("HomePage");
	}

	@Then("click the logout button")
	public void click_the_logout_button() {
		driver.findElement(By.linkText("Log out")).click();
	}
	@Then("The user should taken to the homepage of the demo web shop")
	public void the_user_should_taken_to_the_homepage_of_the_demo_web_shop() {
		System.out.println("HomePage");
	}

	@Then("close the browser")
	public void close_the_browser() {
		driver.quit();
	}

}
